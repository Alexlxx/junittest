import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class OperationsTest
{

    public int Operand1;
    public int Operand2;
    public int Result;
    public String Operation;


    public OperationsTest(int operand1, int operand2, int result, String operation)
    {

        this.Operand1 = operand1;
        this.Operand2 = operand2;
        this.Result = result;
        this.Operation = operation;


    }

    @Parameterized.Parameters(name = "{0}{3}{1} = {2}")
    public static Iterable<Object[]> data()
    {
        ReadFromCsv reader = new ReadFromCsv();
        return reader.getDataFromFile();
    }


    @Test
    public void Operation1()
    {

        if ("+".equals(Operation))
        {
            assertEquals(Result, Operand1 + Operand2);
        } else if ("-".equals(Operation))
        {
            assertEquals(Result, Operand1 - Operand2);
        } else if ("/".equals(Operation))
        {
            assertEquals(Result, Operand1 / Operand2);
        } else if ("/".equals(Operation))
        {
            assertEquals(Result, Operand1 * Operand2);
        }
    }



}