
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ReadFromCsv
{
    public Collection<Object[]> getDataFromFile()
    {
        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));

        String csvFile = System.getProperty("user.dir")+"/Data1.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";

        try
        {

            br = new BufferedReader(new FileReader(csvFile));
            List<Object []> operationsList = new ArrayList<Object[]>();

            while ((line = br.readLine()) != null)
            {
                String[] operationByLine = line.split(cvsSplitBy);

                int firstOperand = Integer.parseInt(operationByLine[0]);
                int secondOperand = Integer.parseInt(operationByLine[1]);
                int result = Integer.parseInt(operationByLine[3]);
                String operation = operationByLine[2];

                operationsList.add(new Object[]{firstOperand, secondOperand, result, operation});

            }

            return operationsList;

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
